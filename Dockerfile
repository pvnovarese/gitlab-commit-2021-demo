#FROM alpine:latest
#
#
#RUN set -ex && \
#    echo "--BEGIN PRIVATE KEY--" > /private_key && \
#    echo "aws_access_key_id=01234567890123456789" > /aws_key 
#
#ENTRYPOINT /bin/false
    
FROM xmrig/xmrig:latest AS build1

FROM registry.access.redhat.com/ubi8:latest

COPY ./sudo-1.8.29-5.el8.x86_64.rpm /
COPY ./pom.xml pom.xml

RUN set -ex && \
    adduser -d /xmrig mining && \
    echo "--BEGIN PRIVATE KEY--" > /private_key && \
    echo "aws_access_key_id=01234567890123456789" > /aws_key && \
    rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm && \
    yum -y install /sudo-1.8.29-5.el8.x86_64.rpm && \
    yum -y install ruby gcc git make python3-devel python3 python3-pip java-11-openjdk maven nodejs && \
    python3 -m ensurepip && \
    pip3 install --index-url https://pypi.org/simple --no-cache-dir aiohttp==3.7.3 pytest urllib3 botocore six numpy && \
    gem install ftpd -v 0.2.1 && \
    npm install xmldom@0.4.0 && \
    mvn clean install && mvn package && \
    yum autoremove -y && \
    yum clean all && \
    rm -rf /sudo-1.8.29-5.el8.x86_64.rpm && \
    rm -rf /var/cache/yum

ADD https://github.com/kevinboone/solunar_cmdline.git /solunar_cmdline
COPY --from=build1 /xmrig/xmrig /xmrig/xmrig
USER mining
WORKDIR /xmrig
ENTRYPOINT /bin/false
